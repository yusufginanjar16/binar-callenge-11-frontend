import React from 'react';
import { Route, Routes } from 'react-router-dom';
import './utils/firebase';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import Landing from './pages/Landing';
import Profile from './pages/Profile';
import ProfileEdit from './pages/ProfileEdit';
import Login from './pages/login/login';
import Game from './pages/Game';
import Games from './pages/Games';
import Register from './pages/login/Register';
import Rank from './pages/Rank';
import Navibar from './components/navibar';
import Footer from './components/footer';

function App() {
  return (
    <div className="App">
      <div className="page-container">
        <div className="content-wrap" />

        <Navibar />

        <Routes>
          <Route
            element={<Profile />}
            path="/players/:id"
          />

          <Route
            element={<ProfileEdit />}
            path="/players/edit/:id"
          />

          <Route
            element={<Game />}
            path="/game/rps"
          />

          <Route
            element={<Games />}
            path="/games"
          />

          <Route
            element={<Login />}
            path="login"
          />

          <Route
            element={<Landing />}
            path="/homepage"
          />

          <Route
            element={<Register />}
            path="/register"
          />

          <Route
            element={<Rank />}
            path="/rank"
          />

          <Route
            element={<Landing />}
            path="/"
          />
        </Routes>

        <Footer />
      </div>
    </div>
  );
}

export default App;
