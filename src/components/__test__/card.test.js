import renderer from 'react-test-renderer';
import { render, screen, cleanup } from '@testing-library/react';
import toBeInTheDocument from '@testing-library/jest-dom/dist/matchers';
import { BrowserRouter } from 'react-router-dom';
import Cards from '../card';

afterEach(() => {
  cleanup();
});

const mockedUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));
test('Card', () => {
  render(
    <BrowserRouter>
      <Cards />
    </BrowserRouter>,
  );
  const cardElement = screen.getByTestId('test-card');
  expect(cardElement).toBeInTheDocument();
  // console.log(cardElement);
});
