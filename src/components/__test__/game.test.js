import renderer from 'react-test-renderer';
import { render, screen, cleanup } from '@testing-library/react';
import toBeInTheDocument from '@testing-library/jest-dom/dist/matchers';
import { BrowserRouter } from 'react-router-dom';
import Game from '../../pages/Game';

afterEach(() => {
  cleanup();
});

test('Game', () => {
  render(
    <BrowserRouter>
      <Game />
    </BrowserRouter>,
  );

  const gamePage = screen.getByTestId('test-game');
  expect(gamePage).toBeInTheDocument();
});

test('Snapshot testing', () => {
  const tree = renderer.create(
    <BrowserRouter>
      <Game />
    </BrowserRouter>,
  ).toJSON;
  expect(tree).toMatchSnapshot();
});
