import renderer from 'react-test-renderer';
import { render, screen, cleanup } from '@testing-library/react';
import toBeInTheDocument from '@testing-library/jest-dom/dist/matchers';
import { BrowserRouter } from 'react-router-dom';
import Footer from '../footer';

afterEach(() => {
  cleanup();
});

test('Footer', () => {
  render(
    <BrowserRouter>
      <Footer />
    </BrowserRouter>,
  );

  const footerElement = screen.getByTestId('test-footer');
  expect(footerElement).toBeInTheDocument();
});
