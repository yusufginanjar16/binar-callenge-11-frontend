import renderer from 'react-test-renderer';
import { render, screen, cleanup } from '@testing-library/react';
import toBeInTheDocument from '@testing-library/jest-dom/dist/matchers';
import { BrowserRouter } from 'react-router-dom';
import Landing from '../../pages/Landing';

afterEach(() => {
  cleanup();
});

test('Landing', () => {
  render(
    <BrowserRouter>
      <Landing />
    </BrowserRouter>,
  );

  const landingPage = screen.getByTestId('test-landing');
  expect(landingPage).toBeInTheDocument();
});
