import renderer from 'react-test-renderer';
import { render, screen, cleanup } from '@testing-library/react';
import toBeInTheDocument from '@testing-library/jest-dom/dist/matchers';
import { BrowserRouter } from 'react-router-dom';
import Navibar from '../navibar';

afterEach(() => {
  cleanup();
});

test('Navbar', () => {
  render(
    <BrowserRouter>
      <Navibar />
    </BrowserRouter>,
  );

  const navbarElement = screen.getByTestId('test-navbar');
  expect(navbarElement).toBeInTheDocument();
});
