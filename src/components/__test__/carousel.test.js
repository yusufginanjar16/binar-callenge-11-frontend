import renderer from 'react-test-renderer';
import { render, screen, cleanup } from '@testing-library/react';
import toBeInTheDocument from '@testing-library/jest-dom/dist/matchers';
import { BrowserRouter } from 'react-router-dom';
import Carousels from '../carousel';

const mockedUsedNavigate = jest.fn();

afterEach(() => {
  cleanup();
});

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));

test('Carousels', () => {
  render(
    <BrowserRouter>
      <Carousels />
    </BrowserRouter>,
  );

  const carouselElement = screen.getByTestId('test-carousel');
  expect(carouselElement).toBeInTheDocument();
});
