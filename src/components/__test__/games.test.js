import renderer from 'react-test-renderer';
import { render, screen, cleanup } from '@testing-library/react';
import toBeInTheDocument from '@testing-library/jest-dom/dist/matchers';
import { BrowserRouter } from 'react-router-dom';
import Games from '../../pages/Games';

afterEach(() => {
  cleanup();
});

test('games', () => {
  render(
    <BrowserRouter>
      <Games />
    </BrowserRouter>,
  );

  const gamesPage = screen.getByTestId('test-games');
  expect(gamesPage).toBeInTheDocument();
});
