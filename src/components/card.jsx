import React from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Cards() {
  const cardInfo = [
    {
      image:
        'https://images.unsplash.com/photo-1511512578047-dfb367046420?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80',
      title: 'Game List',
      text: 'See more awesome games that you can explore to play here ',
      link: '/games',
    },
    {
      image:
        'https://images.unsplash.com/photo-1493711662062-fa541adb3fc8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      title: 'Multiplayer Game',
      text: 'Play some cool games with your friend and gain some points together',
      link: '/',
    },
    {
      image:
        'https://images.unsplash.com/photo-1614332287897-cdc485fa562d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      title: 'Coming Soon',
      text: 'Something cool will happen to make you happy, stay tune!',
      link: '/',
    },
  ];

  const renderCard = (card, index) => (
    <div key={index} className="col ">
      <Card className="w-100 h-100" >
        <Card.Img variant="top" src={card.image} />
        <Card.Body>
          <Card.Title>{card.title}</Card.Title>
          <Card.Text className="text-black text-start">{card.text}</Card.Text>

          <Button variant="dark ">
            <Link to={card.link}>See more</Link>
          </Button>
        </Card.Body>
      </Card>
    </div>
  );
  return (
    <div id="Cards" data-testid="test-card">
      <div
        className="Container d-flex justify-content-center p-5
      "
      >
        <div className="row ">{cardInfo.map(renderCard)}</div>
      </div>
    </div>
  );
}
