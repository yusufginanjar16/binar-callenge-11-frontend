import { useState, useEffect } from 'react';
import { getAuth, onAuthStateChanged, signOut } from 'firebase/auth';
import Container from 'react-bootstrap/Container';

import { Nav, Navbar } from 'react-bootstrap';
import '../App.css';
import { Link, useNavigate } from 'react-router-dom';

export default function Navibar() {
  const [user, setUser] = useState(null);
  const navigate = useNavigate();
  // const auth = getAuth();
  useEffect(() => {
    const user = localStorage.getItem('userId');
    if (user) {
      setUser(user);
    }
  }, []);

  const handleSignOut = async () => {
    try {
      console.log('signing out');
      setUser(null);
      localStorage.removeItem('userId');
      localStorage.removeItem('username');
      localStorage.removeItem('accessToken');
      navigate('/login');
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <div data-testid="test-navbar" style={{ position: 'relative', zIndex: '10' }}>
      <Navbar bg="dark " variant="dark" className="Navbar bg-opacity-75">
        <Container>
          <Link to="/">
            <Navbar.Brand>
              <img
                alt=""
                src="../../assets/icons/binarnew.svg"
                width="30"
                height="30"
                className="d-inline-block align-top Image-rounded "
              />
            </Navbar.Brand>
          </Link>
          <Nav className="me-auto ">
            <Link className='m-2' to="/">
              Home
            </Link>
            <Link className='m-2' to="/game/rps">
              Play RPS
            </Link>
            <Link className='m-2' to="/games">
              Games
            </Link>
            <Link className='m-2' to="/rank">
              Top Scores
            </Link>
          </Nav>
          {user ? (
            <Nav className="justify-content-end">
              <Nav.Link as={Link} to={`/players/${user}`}>
                My Profile
              </Nav.Link>
              <Nav.Link onClick={handleSignOut}>Log Out</Nav.Link>
            </Nav>
          ) : (
            <Nav className="justify-content-end">
              <Nav.Link as={Link} to="/login">
                Login
              </Nav.Link>
              <Nav.Link as={Link} to="/register">
                Sign Up
              </Nav.Link>
            </Nav>
          )}
        </Container>
      </Navbar>
    </div>
  );
}
