import React, { useState } from 'react';
import ReactPlayer from 'react-player';
import Carousel from 'react-bootstrap/Carousel';

export default function Carousels() {
  const [index, setIndex] = useState(0);
  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  const carouselinf = [
    {
      img: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/people-playing-paper-rock-scissors-royalty-free-illustration-1583269312.jpg?crop=0.994xw:0.799xh;0.00160xw,0.195xh&resize=1600:*',
      title: 'Rock Paper Scissors',
      caption:
        '     If you miss your childhood, we provide many traditional games here',
    },
    {
      img: 'https://cdn.oneesports.id/cdn-data/sites/2/2022/04/Xbox-PC-Games-Pass-1-1024x576.webp',
      title: 'Game Pass',
      caption: 'Join the premium pass and experience unlimited games',
    },
    {
      img: 'https://cdn.dribbble.com/users/67858/screenshots/2482202/attachments/1190214/coming_soon.png',
      title: 'Coming Soon',
      caption: "There's something really cool is coming ! Stay tuned",
    },
  ];

  const renderCarousel = (carousel, index) => (
    <Carousel.Item key={index}>
      <img
        className="d-block w-100 h-80"
        src={carousel.img}
        alt="First slide"
      />
      <Carousel.Caption>
        <h3>{carousel.title}</h3>
        <p className="d-flex justify-content-center">{carousel.caption}</p>
      </Carousel.Caption>
    </Carousel.Item>
  );

  return (
    <Carousel
      activeIndex={index}
      onSelect={handleSelect}
      data-testid="test-carousel"
    >
      {carouselinf.map(renderCarousel)}
    </Carousel>
  );
}
