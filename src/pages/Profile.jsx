import {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/profile.css'
import axios from "axios";

export default function Profile() {
    const [player, setPlayer] = useState('');
    const [enableEdit, setEnableEdit] = useState(true);
    const pathname = window.location.pathname
    const getLastItem = thePath => thePath.substring(thePath.lastIndexOf('/') + 1)
    const userId = getLastItem(pathname)

    useEffect(() => {
        axios({
            method: 'get',
            url: 'https://binar-chapter11.herokuapp.com/api/v1/user/' + userId,
        })
            .then(function (response) {
              console.log(response.data.id);
              const data = response.data
              setPlayer({
                    username: data.username,
                    email: data.email,
                    bio: data.bio,
                    score: data.score,
                    level: Math.ceil(data.score / 128) ?? 1,
                    url: data.url != '' && data.url != undefined && data.url != null ? data.url : "https://firebasestorage.googleapis.com/v0/b/fsw22-kelompok1.appspot.com/o/pexels-ron-lach-7848986.jpg?alt=media&token=8a222888-d8f9-4cf6-bc1f-9a744ab0bb5a",
                })
        });
    }, [])

     
    return(
        <div className='profile-page bg-dark'>
            <div className="container">
            <div className="row no-gutters">
                <div className="col-md-4 col-lg-4">
                    <img src={player.url} className="img-fluid" />
                </div>
                <div className="col-md-8 col-lg-8">
                    <div className="d-flex flex-column">
                        <div className="d-flex flex-row justify-content-between align-items-center p-5 bg-info text-white">
                            <h2 className="display-4">{player.username}</h2></div>
                        <div className="p-3 bg-black text-white">
                            <h6>{player.bio}</h6>
                        </div>
                        <div className="d-flex flex-row text-white">
                            <div className="p-4 bg-primary text-center skill-block">
                                <h6>Total Score</h6>
                                <h4>{player.score}</h4>
                            </div>
                            <div className="p-3 bg-success text-center skill-block">
                                <h6>Level</h6>
                                <h4>{player.level}</h4>
                            </div>
                            <div className="p-3 bg-warning text-center skill-block">
                                <h6>Rank</h6>
                                <h4>--</h4>
                            </div>
                            <div className="p-3 bg-danger text-center skill-block">
                            {enableEdit ? <Link className="btn btn-warning font-weight-bold btn-lg text-dark rounded-0" to={ `/players/edit/${userId}` }>EDIT</Link> : ''}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    )
}