import { Formik, Form } from 'formik';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import Swal from 'sweetalert2';
import LoginInput from '../../components/inputs/loginInput';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 2500,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer);
    toast.addEventListener('mouseleave', Swal.resumeTimer);
  },
});

const loginState = {
  email: '',
  password: '',
};

export default function Login() {
  const [login, setLogin] = useState(loginState);
  const { email, password } = login;
  const navigate = useNavigate();

  useEffect(() => {
    // auth.onAuthStateChanged((user) => {
    //   if (user) {
    //     navigate('/homepage');
    //   }
    // });
  }, []);

  const handleOnchangeInput = (e) => {
    const { name, value } = e.target;
    setLogin({
      ...login,
      [name]: value,
    });
  };
  const loginValidation = Yup.object({
    email: Yup.string()
      .required('Email address is required.')
      .email('Must be a valid email.')
      .max(50),
    password: Yup.string().required('password is required'),
  });

  const AuthLogin = async () => {
    try {
      if ((email, password === '')) {
        await Toast.fire({
          icon: 'warning',
          title: 'please fill in the column first',
          timer: 1500,
        });
      } else {
        // const user = await signInWithEmailAndPassword(auth, email, password);
        // console.log(await user.user.getIdToken());

        // login with axios to backend
        const user = await axios.post('https://binar-chapter11.herokuapp.com/api/v1/login', {
          email,
          password,
        });
        if (user.status === 200) {
          await Toast.fire({
            icon: 'success',
            title: 'signed in successfully',
          });
          localStorage.setItem('accessToken', user.data.accessToken);
          localStorage.setItem('username', user.data.data[0].username);
          localStorage.setItem('userId', user.data.data[0].id);
          navigate('/homepage');
        } else {
          await Toast.fire({
            icon: 'error',
            title: 'failed to sign in',
          });
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="container  ">
      <div className="row  justify-content-center   ">
        <Formik
          enableReinitialize
          initialValues={{
            email,
            password,
          }}
          validationSchema={loginValidation}
        >
          {() => (
            <Form>
              <div className="d-flex m-5 justify-content-center ">
                <div className="col-md-6 border border-primary pb-5">
                  <h3 className="text-center pt-5 pb-4">Sign In</h3>
                  <LoginInput
                    label="Email"
                    type="email"
                    name="email"
                    placeholder="Your Email ... "
                    onChange={handleOnchangeInput}
                  />
                  <LoginInput
                    label="Password"
                    type="password"
                    name="password"
                    placeholder="Password Here .."
                    onChange={handleOnchangeInput}
                  />
                  <div className="col-md-7 offset-md-3  d-flex justify-content-start pb-4">
                    <button
                      type="button"
                      className="btn btn-link "
                      onClick={() => {
                        navigate('/forget');
                      }}
                    >
                      Forget Password
                    </button>
                  </div>
                  <div className="col-md-7 offset-md-3 ">
                    <button
                      type="button"
                      className="btn btn-primary col-md-6 "
                      onClick={AuthLogin}
                    >
                      Log In
                    </button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}
