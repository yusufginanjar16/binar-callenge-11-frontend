import React, { useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import Cards from "../components/card";
import Carousels from "../components/carousel";
import "../styles/landing.css";
import ButtonLogout from "../components/buttons/buttonLogOut";
import { signOut } from "firebase/auth";
import { auth } from "../utils/firebase";
import { getDatabase, ref, onValue } from "firebase/database";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import ReactPlayer from "react-player";
import { HashLink as Link } from "react-router-hash-link";
import landingvid from "../asset/landingvid.mp4";

export default function Landing(props) {
  const [player, setPlayer] = useState("");
  useEffect(() => {
      const name = localStorage.getItem("username");
      console.log(name);
      if(name !== null){
        setPlayer(name);
      }
    });

  return (
    <div className="bg-dark" data-testid="test-landing">
      {/* Intro section */}
      <div className="myBG">
        <div className="overlay1"></div>
        {/* <video src={landingvid} autoPlay loop muted aria-disabled /> */}
        <ReactPlayer
          url={landingvid}
          muted
          loop
          playing={true}
          width="100%"
          height={"100vh"}
        />
        <div className=" intro">
          <h1 className="title">Welcome to the Game, {player}</h1>
          <h3 className="d-flex justify-content-center title">
            Let's play some games !
          </h3>
          <Link to="#Cards" smooth>
            <Button variant="outline-light" className=" my-3 w-100 px-5 py-2 ">
              PLAY GAME
            </Button>
          </Link>
        </div>
      </div>
      <div>
        <Carousels />
        <Cards />
      </div>
    </div>
  );
}
